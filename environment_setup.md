# Install

Arch: ```sudo pacman -S go```

### Config & Setup

```go help```

Create go module: ```go mod init io.zzzxt/hello```

This will create a file ```go.mod```, which contains mod metadata and dependency info.

To build and run go module: ```go run .```.

To add dependency, in main go file add ```import {packagename}```, then in mod directory run ```go mod tidy``` which will scan the required dependencies and add them to go.mod file.