### A glimpse

```go
package main

import "fmt"

func main() {
    fmt.Println("Hello, world")
}
```

# Packages

Made up of packages, and programs start running in package "main".

Name is exported if it begins with a capital letter. For example ```fmt.Println()```

# Functions

Notice that Type comes after variable name.

```go
func add(a int, b int) int {
    return a+b
}
```

When two or more consecutive named parameters share a type, you can ignore the type but the last one:

```go
func add(a, b int) int {return a+b}
```

A function can return multiple results like tuple in python

```go
func swap(x, y int) (int, int) {
    return y, x
}

func main() {
    a, b := swap(1, 2)
}
```

The return value can be named as well:

```go
func split(sum int) (x, y int) {
    x = sum * 4 / 9
    y = sum - x
    return
}
```

# Variables

Keyword ```var``` can be used to declare a list of variables, in both function and package level

```go
var c, python, java bool
```

```var``` can also declare variable with initializers

```go
var i, j int = 1, 2
```

Declare with implicit type:

``` go
k := 3
```



Basic types:

```
bool
string
int int8 int16 int32 int64
uint uint{8, 16, 32, 64}, uintptr(for c style pointer arithmetic operations)
byte(alias of uint8)
rune(alias of int32)
float32, float64
complex64 complex128
```



Type conversion: ```T(v)```(like python)

``` go
var i int = 32
var f float = float(i)
```

Note that Go requires explicit type conversion regardless of precission.



Constants: use keyword ```const```

```go
const Pi = 3.141592653
```



# Flow control

for loop:

```go
sum := 0
for i := 0; i<10; i++ {
    sum += 1;
}
```

while  in go:

```go
sum := 0
for sum < 1000 {
    sum += sum
}
```

Infinite loop:

```go
for {
    
}
```



If

```go
if x<0 {
    ...
} else {
    
}
```

With a short statement to execute before the condition

```go
if v:= math.Pow(x, n) < lim {...}
```



Switch:

```go
switch os := runtime.GOOS; os {
    case "darwin":
    	....
    case "linux":
    	'''''
	default:
    	'''''
}
```

Note that go's switch is not exhaustive, that it will only run one branch instead of keep going down until hitting a break.

Also, swithc evaluates from top to down until hitting a succeeded branch.



**defer** Statement

A defer statement defers the execution of a function until the surrounding function returns. The defer statement's arguments are evaluated immediately but won't be executed until the surrounding function ends.

```go
defer fmt.Println(...)
```

 If there is multiple defer statement, they are executed in stack order.



# Pointer

Similar to C, a pointer in go holds a variable's memory address.

Unlike C, pointer arithmetics are prohibited.

```go
i, j := 1, 2

p:= &i     					// p points to i
fmt.Println(p)				// print i's address
fmt.Println(*p)				// print i's value

*p = 4						// Set i through pointer p
```



# Struct

```go
type Vertex struct {
    X int
    Y int
}
v := Vertex{1, 2}
v.X = 2
```

More ways to construct a struct:

```go
var (
    v1 = Vertex{1, 2}
    v2 = Vertex{X: 1, X: 2}
    v3 = Vertex{X: 1}  //Y is default to 0
    v4 = Vertex{}
    p = &v4    //vertex pointer
)
```



# Array

Syntax ```[n]T```: array with n elements of type T

```go
var a [2]string
a[0] = "Hello"
a[1] = "World"
primes := [6]int{2,3,5,7,11,13}
```

Like other languages, arrays can't be resized.



# Slices

A slice is a dynamically-sized, flexible view into the elements of an array. It' like a array literal without the length.

syntax: ```[]T``` (array syntax without size n)

```go
primes := [6]int{2,3,5,7,11,13}
var s []int = primes[1:4]  //[inclusive,exclusive]
s1 := primes[:]  //all elements
s2 := primes[0:4] // 0 to 3
s3 := primes[:3] // 0 to 2
```

Creating a slice literal

```go
[]bool{true, false, true}
```



Length of a slice: number of elements a slice contains. to get a slice's length:  ```len(s)```.

Capacity of a slice: number of elements in the underlying array. to get the capacity: ```cap(s)```.



```nil```: a slice with zero length and capacity, and has no underlying array.



The ```make``` function: a built-in function to create dynamically-sized arrays. The make function allocates a zeroed array and returns a slice that refers to that array:

```go
a := make([]int, 5)  //[0,0,0,0,0] len(a) = 5
```

Also accepts a third argument to specify capacity:

```go
b := make([]int 0, 5)  //len(a) = 0, cap(a) = 5
b = b[:cap(b)]
```



2D slices:

```go
board := [][]string{
    []string{"a", "b", "c"},
    []string{"d", "e", "f"},
}
```



To append to a slice: ```append```: If the backing array is too small, it will create a new larger allocated array.

```go
a := []int{1,2,3}
b := append(a, 4)
```



```range```: a keyword to form a for loop over a slice or map. range returns both the index and the value of each element in the slice:

```go
var pow = []int{1,2,4,8,16,32,64,128}

for i, v := range pow {
    fmt.Println("2**%d = %d\n", i, v)
}
```

You can also skip either value or index:

```go
for i, _ := range pow
for i:= range pow      //both skip values

for _, v := range pow
```



# Maps

syntax ```map[key_type]value_type```.  Zero value is ```Nil```. 

Use ```make``` to create a map:

```go
var m map[string]Vertex

m = make(map[string]Vertex)
```

Or use map literal to create a map with initial key-values:

```go
var m = map[string]Vertex{
    "A": Vertex{1,2},
    "B": Vertex{3,4},
}
```

And if top-level type is just a type name, the type name can be ignored:

```go
var m = map[string]Vertex{
    "A": {1,2},
}
```

Mutating a map:

```go
//Insert or update
m[key] = value

//Retrieve a value
value := m[key]

//Delete a value
delete(m, key)

//Test and retrieve a value
value, ok := m[key]
// if key exists, ok will be true and value is the returned value.
// if key not exists, value is zero value according to the value type, and ok is false.
```



# Function Values

Like lambda in java or closures in Rust, functions can be passed as values.

syntax:```func(input_args) return_args)

```go
func compute(fn func(float64, float64) float64) float64 {
    return fn(3,4)
}
hypot := func(x, y float64) float64 {
    return math.Sqrt(x*x + y*y)
}

fmt.Println(compute(math.Pow))
fmt.Println(compute(hypot))
```



Closures: a go function can reference variables out side its body.

```go
func adder() func(int) int {
    sum := 0
    return func(x int) int {
        sum += x
        return sum
    }
}

func main() {
	pos, neg := adder(), adder()
	for i := 0; i < 10; i++ {
		fmt.Println(
			pos(i),
			neg(-2*i),
		)
	}
}
```



# Method

Go doesn't has classes, but can add methods to a type(like Rust).

Go's method is a function with a special ***Receiver*** argument that lies between the ```func``` keyword and function name:

```go
type Vertex struct {
    X, Y float64
}

func (v Vertex) Abs() float64 {
    return math.Sqrt(v.X*v.X + v.Y*v.Y)
}
```



You can declare methods on non-struct types too, but the declared type must be in the same package with your method.

```go
type MyFloat float64

func (f MyFloat) Abs() float64 {
    if f<0 {
        return float64(-f)
    }
    return float64(f)
}
```



Pointer Receiver: If the receiver is a value, the methods operates on a copy of the type. However, if the receiver is a pointer, then it can directly modify the type object.

```go
type Vertex struct {
    X, Y float64
}

func (v Vertex) Abs() float64 {
    return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

func (v *Vertex) Scale(s float64) {   // Directly modifying the object instead of the copy.
    v.X *= s
    v.Y *= s
}
```



Pointer Indirection

For convenience purpose, if a method has a pointer receiver  and got called with a value(not a pointer), it will be automatically transfered to a pointer.

```go
v: Scale{1,2}
v.Scale(5)   
p := &v
p.scale(5)  // above two lines are equivalent.
```

Similarly, a method with value receiver take either a value or a pointer when they're called:

```go
var v Vertex
v.Abs() //ok
p := &v
p.Abs() //also ok
```



# Interface

Just like in other languages

syntax 

```go
type {Interface_Name} interface{
	//list of method signatures
}
```



Interface Values

Interface values can be represented as a tuple of (value, type) where the value holds the concrete type's data and type is the concrete type's name:

```rust
type I interface {
    M()
}
type T struct {
    S string
}
func (t *T) M() {
    fmt.Println(t.S)
}

var i I
i = &T{"Hello"}
describe(i)    		//(&{Hello}, *main.T)
i.M()				//Hello
```



Empty interface ```interface{}``` : kind of like Object in Java, can hold any type.



Type assertions:

```t := i.(T)```: assert if interface value i holds the concrete type T. If yes, ```t``` will be assigned with underlying T value; If not, the program will panic.

```t, ok := i.(T)```: Similar to above, instead it won't panic if type mismatches. ok will indicate wither i holds type T.



# Errors

```error``` is a built-in interface similar to Stringer:

```fo
type error interface {
	Error() string
}
```

Many functions returns a ```error``` value together with the return value to indicate whether the operation was successful, kind of like java's Optional:

```go
i, err := strconv.Atoi("43")
if err != nil {
    fmt.Println("Couln't convert to string %v", err)
}
```





# Goroutines

A goroutine is a lightweight thread managed by go runtime.

syntax: ```go f(x, y, z)```.

f(x,y,z) is whatever function you want to run in the go routine. The evaluation of x, y, z happened in the calling goroutine and the executio happened in new goroutine.

Goroutines ran in the same memory space, so be cautious of race conditions, memory conflicts...etc.



# Channel

***Typed*** conduit to send and receive values with channel operator ```<-```.

```go
ch <- v    //send v thru ch
v := <- ch //receive from ch and assign to v
```

Similar to arrays, channel must be made:

```go
ch := make(chan int)
```

By default, sends and receives block until the other side is ready.



Buffered channel: like blocking queue

```go
ch := make(chan int, 100)   //Creating channel with buffer size 100
```

In this case, send only blocks when the buffer is full and receive only blocks when channel is empty.



channel lifecycle: senders can explicitly close a channel using ```close()``` function:

```go
close(ch)
```

And receiver and assign additional check flag to check whether a channel is still open:

```
v, ok := <-ch
```

If ok is false, that means the channel is already closed and there will be no more value sent.

Additionally, we can also use ```range``` to keep polling the channel until is closed:

```go
for v := range ch {
    ...
}
```



# Select

The ```select``` keyword lets a goroutine wait on multiple channels until one of them can run, then it executes that case. If multiple channels are available then a random one is chosen. 

The default case in select statement is the default case that will be executed when no other case is ready.

```go
func fibonacci(c, quit chan int) {
	x, y := 0, 1
	for {
		select {
		case c <- x:
			x, y = y, x+y
		case <-quit:
			fmt.Println("quit")
			return
        default:
            time.Sleep(50 * time.Millisecond)
		}
	}
}

func main() {
	c := make(chan int)
	quit := make(chan int)
	go func() {
		for i := 0; i < 10; i++ {
			fmt.Println(<-c)
		}
		quit <- 0
	}()
	fibonacci(c, quit)
}
```



# Mutex

Just like mutx in other languages. In ```sync.Mutex```. Has two methods ```Lock``` and ```Unlock```.

Unlock methods is commonly used with ```defer``` to unlock when function returned.

```go
package main

import (
	"fmt"
	"sync"
	"time"
)

// SafeCounter is safe to use concurrently.
type SafeCounter struct {
	mu sync.Mutex
	v  map[string]int
}

// Inc increments the counter for the given key.
func (c *SafeCounter) Inc(key string) {
	c.mu.Lock()
	// Lock so only one goroutine at a time can access the map c.v.
	c.v[key]++
	c.mu.Unlock()
}

// Value returns the current value of the counter for the given key.
func (c *SafeCounter) Value(key string) int {
	c.mu.Lock()
	// Lock so only one goroutine at a time can access the map c.v.
	defer c.mu.Unlock()
	return c.v[key]
}

func main() {
	c := SafeCounter{v: make(map[string]int)}
	for i := 0; i < 1000; i++ {
		go c.Inc("somekey")
	}

	time.Sleep(time.Second)
	fmt.Println(c.Value("somekey"))
}
```

